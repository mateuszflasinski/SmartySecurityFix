<?php

namespace SmartySecurityFix;

use Shopware\Components\Plugin;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Shopware-Plugin Smarty Securit yFix.
 */
class SmartySecurityFix extends Plugin
{

    /**
    * @param ContainerBuilder $container
    */
    public function build(ContainerBuilder $container)
    {
        $container->setParameter('smarty_security_fix.plugin_dir', $this->getPath());
        parent::build($container);
    }

}
