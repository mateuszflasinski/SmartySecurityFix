<?php
namespace SmartySecurityFix\Subscriber;

use Enlight\Event\SubscriberInterface;

/**
 * Class Smarty
 * @package SmartySecurityFix\Subscriber
 */
class Smarty implements SubscriberInterface
{
    /** @var string */
    private $pluginDir;

    /**
     * Smarty constructor.
     *
     * @param $pluginDir
     */
    public function __construct($pluginDir)
    {
        $this->pluginDir = $pluginDir;
    }

    /**
     * {@inheritdoc]
     */
    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Bootstrap_InitResource_template' => 'onInitTemplate',
        ];
    }

    /**
     * Register custom plugin dir
     */
    public function onInitTemplate()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            define('SMARTY_SYSPLUGINS_DIR', $this->pluginDir);
        }
    }
}
