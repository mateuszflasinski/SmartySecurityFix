# Smarty Security Fix

The plugin corrects the error
    
    Fatal error: Uncaught Error: Call to undefined method stdClass::isTrustedResourceDir() in engine/Library/Smarty/sysplugins/smarty_internal_resource_file.php:33
    
by changing

    if (is_object($source->smarty->security_policy)) {
        $source->smarty->security_policy->isTrustedResourceDir($source->filepath);
    }
    
on

    if (is_object($source->smarty->security_policy) && method_exists($source->smarty->security_policy,'isTrustedResourceDir')) {
        $source->smarty->security_policy->isTrustedResourceDir($source->filepath);
    }
    
in the file custom/plugins/SmartySecurityFix/Components/Smarty/sysplugins/smarty_internal_resource_file.php on the line 32
In order to replace one file, I copied all the smarty plugins because I did not want to think how to replace one.